import 'package:alwafiscoreboard/helper/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'pages/home.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.manual,
    overlays: [SystemUiOverlay.bottom],
  );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Alwafi Scoreboard',
      theme: ThemeData(
        primarySwatch: MaterialColor(primaryColorCode, color),
      ),
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
