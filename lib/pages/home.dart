import 'package:alwafiscoreboard/helper/colors.dart';
import 'package:alwafiscoreboard/helper/styles.dart';
import 'package:alwafiscoreboard/models/player_data.dart';
import 'package:alwafiscoreboard/pages/menu.dart';
import 'package:alwafiscoreboard/widgets/container_shadow_circular.dart';
import 'package:alwafiscoreboard/widgets/score.dart';
import 'package:alwafiscoreboard/widgets/stopwatch.dart';
import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  void onTapScore(dynamic table) {
    setState(() {
      table['score']++;
    });
  }

  void onTapMin(dynamic table) {
    setState(() {
      if (table['score'] > 0) {
        table['score']--;
      }
    });
  }

  Widget topWidgetSizedBox = SizedBox(width: 3.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () => onTapScore(scoreTable[1]),
                      child: Score(
                        id: 1,
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () => onTapScore(scoreTable[2]),
                      child: Score(
                        id: 2,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //Setting Menu
              Align(
                alignment: Alignment.topCenter,
                child: ContainerShadowCircular(
                  height: 50.0,
                  width: 70.0,
                  topLeftRadius: 0.0,
                  topRightRadius: 0.0,
                  bottomLeftRadius: 10.0,
                  bottomRightRadius: 10.0,
                  childWidget: GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return MenuPage();
                          },
                        ),
                      );
                      setState(() {});
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 3.0),
                      child: Icon(
                        Icons.settings,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              topWidgetSizedBox,

              //Player A Menu
              Align(
                alignment: Alignment.topCenter,
                child: ContainerShadowCircular(
                  height: 50.0,
                  width: 100.0,
                  topLeftRadius: 0.0,
                  topRightRadius: 0.0,
                  bottomLeftRadius: 10.0,
                  bottomRightRadius: 10.0,
                  childWidget: GestureDetector(
                    onTap: () {},
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 3.0),
                      child: Marquee(
                        text: scoreTable[1]['name'],
                        blankSpace: 50.0,
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              topWidgetSizedBox,

              //vs Menu
              Align(
                alignment: Alignment.topCenter,
                child: ContainerShadowCircular(
                  height: 50.0,
                  width: 58.0,
                  topLeftRadius: 0.0,
                  topRightRadius: 0.0,
                  bottomLeftRadius: 10.0,
                  bottomRightRadius: 10.0,
                  childWidget: FittedBox(
                    child: Text(
                      'vs',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ),
              ),
              topWidgetSizedBox,

              //Player B Menu
              Align(
                alignment: Alignment.topCenter,
                child: ContainerShadowCircular(
                  height: 50.0,
                  width: 100.0,
                  topLeftRadius: 0.0,
                  topRightRadius: 0.0,
                  bottomLeftRadius: 10.0,
                  bottomRightRadius: 10.0,
                  childWidget: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 3.0),
                    child: Marquee(
                      text: scoreTable[2]['name'],
                      blankSpace: 50.0,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              topWidgetSizedBox,

              //Choice Menu
              Align(
                alignment: Alignment.topCenter,
                child: ContainerShadowCircular(
                  height: 50.0,
                  width: 70.0,
                  topLeftRadius: 0.0,
                  topRightRadius: 0.0,
                  bottomLeftRadius: 10.0,
                  bottomRightRadius: 10.0,
                  childWidget: GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            titlePadding: EdgeInsets.zero,
                            contentPadding: EdgeInsets.zero,
                            title: Container(
                              padding: EdgeInsets.all(10.0),
                              color: primaryColor,
                              alignment: Alignment.center,
                              child: Text(
                                'Game Control Panel',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            content: Container(
                              color: Colors.black,
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 5.0,
                                    ),
                                    constraints: BoxConstraints(
                                      maxWidth: 100.0,
                                    ),
                                    child: ListTile(
                                      onTap: () {},
                                      title: Icon(
                                        Icons.next_plan_outlined,
                                        color: Colors.white,
                                      ),
                                      subtitle: FittedBox(
                                        child: Text(
                                          'Next Round',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 5.0,
                                    ),
                                    constraints: BoxConstraints(
                                      maxWidth: 100.0,
                                    ),
                                    child: ListTile(
                                      onTap: () {},
                                      title: Icon(
                                        Icons.restart_alt,
                                        color: Colors.white,
                                      ),
                                      subtitle: FittedBox(
                                        child: Text(
                                          'Reset Game',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 5.0,
                                    ),
                                    constraints: BoxConstraints(
                                      maxWidth: 100.0,
                                    ),
                                    child: ListTile(
                                      onTap: () {},
                                      title: Icon(
                                        Icons.swap_horiz,
                                        color: Colors.white,
                                      ),
                                      subtitle: FittedBox(
                                        child: Text(
                                          'Swap Player',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 3.0),
                      child: Icon(
                        Icons.more_horiz,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ContainerShadowCircular(
              topLeftRadius: 10.0,
              topRightRadius: 10.0,
              bottomLeftRadius: 0.0,
              bottomRightRadius: 0.0,
              childWidget: StopWatch(),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: ContainerShadowCircular(
              topLeftRadius: 0.0,
              topRightRadius: 10.0,
              bottomLeftRadius: 0.0,
              bottomRightRadius: 0.0,
              childWidget: GestureDetector(
                onTap: () => onTapMin(scoreTable[1]),
                child: FittedBox(
                  child: Text(
                    '-1',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: ContainerShadowCircular(
              topLeftRadius: 10.0,
              topRightRadius: 0.0,
              bottomLeftRadius: 0.0,
              bottomRightRadius: 0.0,
              childWidget: GestureDetector(
                onTap: () => onTapMin(scoreTable[2]),
                child: FittedBox(
                  child: Text(
                    '-1',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
