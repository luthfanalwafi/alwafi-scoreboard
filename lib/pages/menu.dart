import 'package:alwafiscoreboard/helper/colors.dart';
import 'package:alwafiscoreboard/helper/menu.dart';
import 'package:alwafiscoreboard/widgets/edit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  State<MenuPage> createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  @override
  void initState() {
    super.initState();
    setOrientation();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void setOrientation() {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  Future<bool> onWillpop() async {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft]);
    return true;
  }

  Future onTap(int id) async {
    final result = await showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      isScrollControlled: true,
      builder: (context) {
        return EditTextForm();
      }
    );
    if (result != null) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillpop,
      child: Scaffold(
        backgroundColor: secondaryColor,
        appBar: AppBar(
          title: Text('Settings'),
        ),
        body: ListView.builder(
          itemCount: menuList.length,
          itemBuilder: (contex, index) {
            return ListTile(
              contentPadding: EdgeInsets.symmetric(
                vertical: 5.0,
                horizontal: 20.0,
              ),
              title: Text(
                menuList[index]['title'],
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              subtitle: Text(
                menuList[index]['subtitle'],
                style: TextStyle(
                  color: Colors.grey[400],
                ),
              ),
              leading: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    menuList[index]['icon'],
                    color: Colors.white,
                  ),
                ],
              ),
              trailing: Icon(
                Icons.chevron_right,
                color: Colors.white,
              ),
              onTap: () => onTap(menuList[index]['id']),
            );
          },
        ),
      ),
    );
  }
}
