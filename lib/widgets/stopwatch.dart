import 'package:alwafiscoreboard/helper/snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

class StopWatch extends StatefulWidget {
  const StopWatch({Key? key}) : super(key: key);

  @override
  _StopWatchState createState() => _StopWatchState();
}

class _StopWatchState extends State<StopWatch> {
  bool isTimerStart = false;
  final StopWatchTimer _stopWatchTimer = StopWatchTimer(
    mode: StopWatchMode.countUp,
  );

  void onTapTimer() {
    if (!isTimerStart) {
      _stopWatchTimer.onExecute.add(StopWatchExecute.start);
    } else {
      _stopWatchTimer.onExecute.add(StopWatchExecute.stop);
    }
    isTimerStart = !isTimerStart;
    showSnackBar(context, 'Tap 2x untuk reset');
  }

  void onResetTimer() {
    if (isTimerStart) {
      isTimerStart = !isTimerStart;
      _stopWatchTimer.onExecute.add(StopWatchExecute.stop);
    }
    _stopWatchTimer.onExecute.add(StopWatchExecute.reset);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapTimer,
      onDoubleTap: onResetTimer,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 10.0,
        ),
        child: StreamBuilder<int>(
          stream: _stopWatchTimer.rawTime,
          initialData: _stopWatchTimer.rawTime.value,
          builder: (context, snap) {
            final value = snap.data!;
            final displayTime = StopWatchTimer.getDisplayTime(
              value,
              hours: true,
            );
            return FittedBox(
              child: Text(
                displayTime,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
