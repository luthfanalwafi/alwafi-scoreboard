import 'package:alwafiscoreboard/models/player_data.dart';
import 'package:flutter/material.dart';

class Score extends StatelessWidget {
  final int? id;

  const Score({
    Key? key,
    this.id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final table = scoreTable[id];

    return Container(
      height: screenHeight,
      color: table['color'],
      child: FittedBox(
        child: Center(
          child: Text(
            table['score'].toString(),
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
