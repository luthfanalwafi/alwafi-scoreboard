import 'package:flutter/material.dart';

class ContainerShadowSharp extends StatelessWidget {
  final Widget? childWidget;
  final double? topLeftRadius;
  final double? topRightRadius;
  final double? bottomLeftRadius;
  final double? bottomRightRadius;

  const ContainerShadowSharp({
    Key? key,
    required this.childWidget,
    required this.topLeftRadius,
    required this.topRightRadius,
    required this.bottomLeftRadius,
    required this.bottomRightRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyCustomClipper(),
      child: Container(
        padding: EdgeInsets.all(15.0),
        height: 50.0,
        width: 200.0,
        color: Colors.black.withOpacity(0.5),
        child: childWidget,
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path()
        ..lineTo(size.width, 0)
        ..lineTo(size.width * 0.9, size.height)
        ..lineTo(0, size.height)
        ..close();
     
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
