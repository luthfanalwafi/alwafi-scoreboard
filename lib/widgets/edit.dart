import 'package:alwafiscoreboard/helper/colors.dart';
import 'package:alwafiscoreboard/helper/custom_button.dart';
import 'package:alwafiscoreboard/helper/styles.dart';
import 'package:alwafiscoreboard/models/player_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EditTextForm extends StatefulWidget {
  const EditTextForm({
    Key? key,
  }) : super(key: key);

  @override
  State<EditTextForm> createState() => _EditTextFormState();
}

class _EditTextFormState extends State<EditTextForm> {
  final formKey = GlobalKey<FormState>();
  TextEditingController aController = TextEditingController();
  TextEditingController bController = TextEditingController();
  String? playerA = '', playerB = '';

  @override
  void initState() {
    super.initState();
    aController.text = scoreTable[1]['name'];
    bController.text = scoreTable[2]['name'];
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Container(
        padding: MediaQuery.of(context).viewInsets,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                'Edit',
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 30.0,
                vertical: 10.0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    controller: aController,
                    decoration: kMessageTextField.copyWith(
                      hintText: '',
                      prefixIcon: Container(
                        padding: EdgeInsets.only(
                          left: 15.0,
                          right: 5.0,
                        ),
                        child: Text(
                          'Player A : ',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      prefixIconConstraints: BoxConstraints(
                        minWidth: 0,
                        minHeight: 0,
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]')),
                    ],
                    onChanged: (value) {
                      setState(() {
                        playerA = value;
                      });
                    },
                    validator: (value) {
                      if (value == '') {
                        return 'Please insert player name';
                      }
                    },
                  ),
                  SizedBox(height: 20.0),
                  TextFormField(
                    controller: bController,
                    decoration: kMessageTextField.copyWith(
                      hintText: '',
                      prefixIcon: Container(
                        padding: EdgeInsets.only(
                          left: 15.0,
                          right: 5.0,
                        ),
                        child: Text(
                          'Player B : ',
                          textAlign: TextAlign.center,
                        ),
                      ),
                      prefixIconConstraints: BoxConstraints(
                        minWidth: 0,
                        minHeight: 0,
                      ),
                    ),
                    keyboardType: TextInputType.text,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[a-z A-Z]')),
                    ],
                    onChanged: (value) {
                      setState(() {
                        playerB = value;
                      });
                    },
                    validator: (value) {
                      if (value == '') {
                        return 'Please insert player name';
                      }
                    },
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10.0,
                bottom: 30.0,
              ),
              child: CustomButton(
                color: greenColor,
                text: 'SAVE',
                textColor: Colors.white,
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    scoreTable[1]['name'] = playerA!;
                    scoreTable[2]['name'] = playerB!;
                    Navigator.of(context).pop(true);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
