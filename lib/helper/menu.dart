import 'package:alwafiscoreboard/models/player_data.dart';
import 'package:flutter/material.dart';

List menuList = [
  {
    'id': 0,
    'title': 'Change player name',
    'subtitle': '${scoreTable[1]['name']}, ${scoreTable[2]['name']}',
    'icon': Icons.person,
    'onPressed': () {},
  },
  {
    'id': 1,
    'title': 'Change player color',
    'subtitle': '$colorA, $colorB',
    'icon': Icons.color_lens,
    'onPressed': () {},
  },
];
