import 'package:flutter/material.dart';

Color defaultColor = Colors.red;
Color primaryColor = Color(0xff1c1b1b);
Color secondaryColor = Color(0xff2a2727);
Color redColor = Colors.red;
Color blueColor = Colors.blue;
Color greenColor = Colors.green;
Color yellowCollor = Colors.yellow;
Color pinkColor = Colors.pink;
Color purpleColor = Colors.purple;
Color orangeColor = Colors.orange;
Color indigoColor = Colors.indigo;

int primaryColorCode = 0xff1c1b1b;
int brownColorCode = 0xff52331f;
int sdOrangeColorCode = 0xffff5100;
int sdYellowColorCode = 0xffe4bb17;
int sdGreenColorCode = 0xff5bc33c;

Map<int, Color> color = {
  50: Color.fromRGBO(51, 153, 255, .1),
  100: Color.fromRGBO(51, 153, 255, .2),
  200: Color.fromRGBO(51, 153, 255, .3),
  300: Color.fromRGBO(51, 153, 255, .4),
  400: Color.fromRGBO(51, 153, 255, .5),
  500: Color.fromRGBO(51, 153, 255, .6),
  600: Color.fromRGBO(51, 153, 255, .7),
  700: Color.fromRGBO(51, 153, 255, .8),
  800: Color.fromRGBO(51, 153, 255, .9),
  900: Color.fromRGBO(51, 153, 255, 1),
};

Color colorString(String color) {
  switch (color) {
    case 'red':
      return redColor;
    case 'blue':
      return blueColor;
    case 'green':
      return greenColor;
    case 'yellow':
      return yellowCollor;
    default:
      return redColor;
  }
}
