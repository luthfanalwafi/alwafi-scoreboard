import 'package:alwafiscoreboard/helper/colors.dart';

String playerNameA = 'A';
String playerNameB = 'B';
String colorA = 'red';
String colorB = 'blue';
Map<int, dynamic> scoreTable = {
  1: {
    'score': 0,
    'color': colorString(colorA),
    'name': playerNameA,
  },
  2: {
    'score': 0,
    'color': colorString(colorB),
    'name': playerNameB,
  },
};
